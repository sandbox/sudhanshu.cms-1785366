-- SUMMARY--

When a user copys a text from website, The OnCopy Module appends the site URL and other user defined text on the clipboard along with the selected text.
It helps in impriving SEO of website.


-- REQUIREMENTS --
None.


--INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

configure the text that should be appended in Administration -> Site configuration > Configure OnCopy